#use wml::debian::translation-check translation="326ce30b40d6fb0de9a5955b4d8b0d7b4d143303" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ansible, un système de
gestion de configuration, de déploiement et d'exécution de tâches :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10855">CVE-2018-10855</a>
/ <a href="https://security-tracker.debian.org/tracker/CVE-2018-16876">CVE-2018-16876</a>

<p>L'indicateur de tâche no_log n'était pas respecté avec pour conséquence
une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10875">CVE-2018-10875</a>

<p>ansible.cfg était lu à partir du répertoire de travail courant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16837">CVE-2018-16837</a>

<p>Le module utilisateur divulguait les paramètres passés à ssh-keygen vers
l'environnement des processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3828">CVE-2019-3828</a>

<p>Le module de récupération était vulnérable à une traversée de
répertoires.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 2.2.1.0-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ansible.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ansible, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ansible">\
https://security-tracker.debian.org/tracker/ansible</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4396.data"
# $Id: $
