#use wml::debian::translation-check translation="2f2e4e4ed7b781eff447b99c3d177c672b61e21f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Tim Starling a découvert deux vulnérabilités dans firejail, un programme
de bac à sable pour restreindre l'environnement d'exécution d'applications
non approuvées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

<p>firejail ne respecte pas le séparateur de fin d'options (« -- »),
permettant à un attaquant doté du contrôle sur les options de la ligne de
commande de l'application mise dans le bac à sable d'écrire des données
dans un fichier spécifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

<p>firejail lors de la redirection d'une sortie au moyen de --output ou de
--output-stderr, concaténait tous les paramètres de la ligne de commande en
une chaîne unique passée à l'interpréteur de commande. Un attaquant doté du
contrôle sur les paramètres de la ligne de commande de l'application mise dans le bac à sable
pourrait tirer avantage de ce défaut pour exécuter d'autres commandes
arbitraires.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 0.9.58.2-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firejail.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firejail, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firejail">\
https://security-tracker.debian.org/tracker/firejail</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4742.data"
# $Id: $
