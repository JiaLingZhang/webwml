#use wml::debian::translation-check translation="308d9cde017e3cfda5dd5d48e62616cc728c72c4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans Redcarpet avant sa version 3.5.1, il existait une vulnérabilité
d’injection pouvant permettre une attaque par script intersite.</p>

<p>Dans les versions touchées, aucune protection d’HTML n’était réalisée lors du
traitement de guillemets. Cela s’appliquait même si l’option <q>:escape_html</q>
était utilisée.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.3.4-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-redcarpet.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-redcarpet, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-redcarpet">https://security-tracker.debian.org/tracker/ruby-redcarpet</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2526.data"
# $Id: $
