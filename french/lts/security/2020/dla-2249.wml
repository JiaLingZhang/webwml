#use wml::debian::translation-check translation="86c9e7001580f2f7605360ecd0f38d826d0c7d9b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés à l’encontre de src:libexif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0182">CVE-2020-0182</a>

<p>Dans exif_entry_get_value de exif-entry.c, une lecture hors limites est
possible à cause d’une vérification manquante de limites. Cela pourrait conduire
à une divulgation locale d'informations sans nécessité de privilèges
supplémentaires. Une interaction utilisateur n’est pas nécessaire pour son
exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0198">CVE-2020-0198</a>

<p>Dans exif_data_load_data_content de exif-data.c, une interruption de
nettoyage (UBSAN) est possible à cause d’un dépassement d'entier. Cela pourrait
conduire à un déni de service distant sans nécessité de privilèges
supplémentaires. Une interaction utilisateur est nécessaire pour son
exploitation.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.6.21-2+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libexif.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2249.data"
# $Id: $
