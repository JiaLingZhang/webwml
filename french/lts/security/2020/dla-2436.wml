#use wml::debian::translation-check translation="490964305ec8d74f63f1c7afe3349e678d5aac56" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans le gestionnaire
d’affichage sddm par lequel des utilisateurs locaux non privilégiés pourraient
créer une connexion sur le serveur X.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28049">CVE-2020-28049</a>

<p>Un problème a été découvert dans SDDM avant sa version 0.19.0. Celui-ci
démarrait de manière incorrecte le serveur X de telle façon que, pendant une
courte période, des utilisateurs locaux non privilégiés pouvaient créer une
connexion sur le serveur X sans fournir l’authentification appropriée. Un
attaquant local pouvait donc accéder au contenu de l’affichage du serveur X et,
par exemple, intercepter les frappes de clavier ou accéder au presse-papier.
Cela était provoqué par une situation de compétition lors de la création du
fichier Xauthority.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.14.0-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sddm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2436.data"
# $Id: $
