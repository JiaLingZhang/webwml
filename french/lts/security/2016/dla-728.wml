#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet
Tomcat et le moteur JSP qui pourraient avoir pour conséquence de possibles
attaques temporelles pour déterminer des noms d'utilisateurs valables, le
contournement du gestionnaire de sécurité, la divulgation de propriétés du
système, un accès non restreint aux ressources globales, l'écrasement de
fichiers arbitraires et éventuellement une élévation de privilèges.</p>

<p>En complément, cette mise à jour renforce encore les scripts de
démarrage et du responsable de Tomcat pour empêcher de possibles élévations
de privilèges. Merci à Paul Szabo pour le rapport.</p>

<p>Il s'agit probablement de la dernière mise à jour de sécurité de
Tomcat 6 qui atteindra sa fin de vie dans un mois exactement. Nous
recommandons fortement de passer à une autre version prise en charge telle
que Tomcat 7 dès que possible.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 6.0.45+dfsg-1~deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat6.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-728.data"
# $Id: $
