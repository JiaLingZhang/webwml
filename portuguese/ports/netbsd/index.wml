#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#include "$(ENGLISHDIR)/ports/netbsd/menu.inc"
#use wml::debian::translation-check translation="6a11f166d66c3d21d75c0b38c728ed3d3dd75ba8"
{#style#:
<style type="text/css">
    .update {
	color:		red;
	background:	inherit;
	font-weight:	bold;
    }
</style>
:##}

<define-tag update endtag=required>
  <p><span class="update">UPDATE: </span>
    %body
  </p>
</define-tag>


#############################################################################
<div class="important">
<p><strong>
Este trabalho de porte foi abandonado faz tempo. Ele não tem atualizações desde
outubro de 2002. A informação nesta página é somente para propósitos históricos
</strong></p>
</div>


<h1>
Debian GNU/NetBSD
</h1>


<p>
O Debian GNU/NetBSD é um porte do sistema operacional Debian para o
kernel NetBSD e a libc (não confundir com outros portes Debian BSD
baseados em glibc). Ele atualmente está em um estágio inicial de
desenvolvimento - contudo, agora ele pode ser instalado do zero.
</p>

# link morto <p>
#<a href="http://www.srcf.ucam.org/debian-netbsd/floppies">\
#Download experimental install floppies</a> (last
#updated 6th October 2002)
#</p>

<p>
<a href="why">Por que Debian GNU/NetBSD?</a>
</p>

<h2>
Como instalar
</h2>

<p>
Faça o download das imagens de disquete do link acima. Para laptops, use as
imagens de laptop - para todas as outras máquinas, use as imagens normais.
Escreva essas imagens nos disquetes. Faça o boot no primeiro disco - você será
solicitado(a) a trocar os discos. Uma vez que o sistema de menu apareça, siga
as instruções que forem apresentadas
a você.
</p>

<h2>
A fazer
</h2>

<p>
Pacotes que precisam ser produzidos
</p>

<ul>
<li>
qualquer uma das bibliotecas em <kbd>/lib</kbd> ou <kbd>/usr/lib</kbd> que
atualmente não estão empacotadas, e precisam ser
</li>
<li>
  base-passwd está desesperadamente infeliz
  <update>
    Nós agora temos um base-passwd que basicamente funciona para FreeBSD e
    NetBSD (modulo a segfault). Obrigado(a) ao Nathan e Matthew.
  </update>
</li>
<li>equivalentes ao console-tools/data precisam ser produzidos
    <update>
      Pacotes que fornecem funcionalidades básicas precisam ser produzidos
    </update>
</li>
<li>
O netbase precisa ser reconstruído. Este é provavelmente um dos mais
desajeitados - nós temos o código-fonte para as versões BSD do ifconfig e do
resto, mas as semânticas são meio que diferentes. Se nós ficarmos com a
semântica do BSD, nós teremos que lidar com qualquer script que assuma
semânticas do estilo do Linux. O Hurd segue a semântica do estilo do Linux,
e se não, como eles(as) lidaram com isso?
  <update>
     Marcus Brinkmann do time Hurd
     <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00087.html">\
     esclareceu</a> isto um pouco e rascunhou possíveis soluções. A abordagem
     atual é usar as ferramentas NetBSD e modificar ifupdown de modo a
     fornecer a mesma interface para o(a) usuário(a).
  </update>
</li>
<li>procps (provavelmente melhor somente fornecer as versões BSD)</li>
# link dead <li>sysklogd
#  (we can probably use <a
#  href="https://packages.debian.org/msyslog">msyslog</a> instead)
#
#  <update>
#     <a href="https://packages.debian.org/msyslog">msyslog</a> works on
#     NetBSD (modulo some hickups related to paths of files)
#  </update>
#</li>
<li>sysvinit
  (O BSD init não suporta runlevels. Nós podemos modificá-lo para funcionar
  com o Debian, com um único runlevel sem muitos problemas)
  <update>
    sysvinit está ativo e rodando, o Matthew conseguiu fazer boot nativamente
    no Debian GNU/NetBSD em um i386!  Ainda existem algumas pequenas falhas
    com scripts de boot wrt., mas é um passo importante
    em direção a um sistema totalmente operável.
  </update>
</li>
<li><a href="https://packages.debian.org/fakeroot">fakeroot</a>
  <update>
    Fakeroot agora funciona.
  </update>
</li>
<li>XFree86
    (Nathan está trabalhando nisso atualmente e descobriu que o
    <a href="https://packages.debian.org/ed">ed</a> é necessário, com
    segfaults.  Muitas pessoas estão investigando este problema).

    <update>
        O ed funciona quando construído com libed.a. Além disso, citando Joel:
	<q>X11 está em um estado funcional</q>!  Não está empacotado
	adequadamente, mas funciona. Aguarde pacotes em breve.
    </update>
</li>
<li>gcc-3.0
    (Nem gcc-3.0.1 nem gcc-current estão em um estado utilizável para o
    NetBSD no momento. O Joel tem uma versão funcionando do gcc-current
    e postou os <a
    href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00137.html">resultados
    da suíte de testes</a>.  libstdc++ ainda está muito infeliz).

    <update>
        O gcc-3.0.4 foi lançado e agora <a
        href="http://gcc.gnu.org/gcc-3.0/features.html">suporta
        sistemas NetBSD ELF</a> (ao menos para o alvo x86).
    </update>
</li>
# link dead <li>How to handle architectures?<br />
#    There is currently an on-going <a
#    href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00033.html">discussion</a>
#    on the mailing list about Marcus Brinkmann's <a
#    href="http://master.debian.org/~brinkmd/arch-handling.txt">arch-handling
#    proposal</a>.
#</li>
</ul>


<h2>
Recursos
</h2>

<ul>
<li><a href="$(HOME)/">Debian</a></li>

<li><a href="http://www.netbsd.org/">NetBSD</a></li>

# link dead <li>
#<a href="http://www.srcf.ucam.org/debian-netbsd/">\
#Matthew's apt-gettable package archive</a>
#</li>

# link dead <li>
#<a href="http://debian-bsd.lightbearer.com/">Joel's apt-gettable archive</a>
#of packages for FreeBSD and NetBSD
#</li>

# link dead <li>
# <a href="ftp://trantor.utsl.org/pub/">A FreeBSD based chroot
# environment and some packages</a>
# </li>

# link dead <li>
#<a href="http://debian-bsd.lightbearer.com/debian-gnu-freebsd.tar.bz2">Another
#FreeBSD chroot tarball</a> and <a
#href="http://debian-bsd.lightbearer.com/kernel-ext2.tar.gz">kernel
#with ext2 support</A>, both built by
#<a href="mailto:rmh@debian.org">Robert Millan</a>.
#</li>

# link dead <li>
# <a href="http://master.debian.org/~dexter/debian-freebsd/">Debian
# GNU/FreeBSD packages</a> (very old, based on FreeBSD-3.3 and slink)
# </li>
</ul>

<p>
Existe uma lista de discussão Debian GNU/*BSD. Envie um e-mail (em inglês) para
<a href="mailto:debian-bsd-request@lists.debian.org?subject=subscribe">\
debian-bsd-request@lists.debian.org</a> com subscribe como assunto para
que você seja inscrito(a). Os arquivos estão disponíveis em
<url "https://lists.debian.org/debian-bsd/" />.
</p>

<hr />
<p>
Para contatar o time Debian GNU/NetBSD, envie um e-mail (em inglês) para
<email "debian-bsd@lists.debian.org" />.
Comentários, questões ou sugestões relativos a nossa seção do site web do
Debian também são bem-vindos nesse endereço.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
