#use wml::debian::template title="Sites de espelho mundiais do Debian" BARETITLE=true
#use wml::debian::translation-check translation="346f82cf48a105b4686a44b1d3a4fe7a11adc9c0"

<p>Esta é uma lista <strong>completa</strong> dos espelhos do Debian.
Para cada site, os diferentes tipos de materiais disponíveis são listados,
junto com o método de acesso para cada tipo.</p>

<p>As seguintes coisas são espelhadas:</p>

<dl>
<dt><strong>Pacotes</strong></dt>
	<dd>O depósito de pacotes do Debian.</dd>
<dt><strong>Imagens do CD</strong></dt>
	<dd>Imagens oficiais de CD do Debian. Veja
	<url "https://www.debian.org/CD/"> para detalhes.</dd>
<dt><strong>Lançamentos antigos</strong></dt>
	<dd>O repositório de versões antigas do Debian que foram lançadas.
	<br />
        Algumas das versões antigas também incluíam o chamado repositório
        debian-non-US, com seções para pacotes Debian que não podiam ser
        distribuídos nos Estados Unidos devido a patentes de software ou
        devido ao uso de encriptação. As atualizações do debian-non-US
        foram descontinuadas com o Debian 3.1.</dd>

</dl>

<p>Os seguintes métodos de acesso são possíveis:</p>

<dl>
<dt><strong>HTTP</strong></dt>
	<dd>Acesso web padrão, mas pode ser usado para fazer o download de arquivos.</dd>
<dt><strong>rsync</strong></dt>
	<dd>Um meio eficiente de espelhamento.</dd>
</dl>

<p>A entrada <q>tipo</q> é uma dessas:</p>

<dl>
<dt><strong>leaf</strong></dt>
	<dd>Compreendem a maior parte dos espelhos.</dd>
<dt><strong>Push-Secondary</strong></dt>
	<dd>Esses sites espelham diretamente de um site Push-Primary, usando
	espelhamento push.</dd>
<dt><strong>Push-Primary</strong></dt>
	<dd>Esses sites espelham diretamente de um site de repositório master
	(os quais não são acessíveis publicamente), usando espelhamento
        push.</dd>
</dl>

<p>(Veja <a href="https://www.debian.org/mirror/push_mirroring">a página sobre espelhamento push</a>
para detalhes).</p>

<p>A cópia confiável da seguinte lista sempre pode ser encontrada em:
<url "https://www.debian.org/mirror/list-full">.
<br />
Tudo mais que você queira saber sobre os espelhos do Debian:
<url "https://www.debian.org/mirror/">.
</p>

<hr style="height:1">

<p>Pule diretamente para um país na lista:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"
