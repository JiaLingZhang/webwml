#use wml::debian::template title="Debian &ldquo;lenny&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/lenny/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Debian GNU/Linux installeren <current_release_lenny></h1>

<div class="important">
<p><strong>Debian GNU/Linux 5.0 werd vervangen door
<a href="../../squeeze/">Debian GNU/Linux 6.0 (<q>squeeze</q>)</a>.
Sommige van deze installatie-images zijn mogelijk niet langer beschikbaar of
werken niet meer. Het wordt aanbevolen om in de plaats daarvan squeeze te
installeren.
</strong></p>
</div>

<p>
<strong>Voor de installatie van Debian GNU/Linux</strong> <current_release_lenny>
(<em>lenny</em>), kunt u een van de volgende images downloaden:
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst cd-image (meestal 135-175 MB)</strong></p>
		<netinst-images />
</div>

<div class="item col50 lastcol">
	<p><strong>visitekaart-cd-image (meestal 20-50 MB))</strong></p>
		<businesscard-images />
</div>

</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige cd-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige dvd-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>

<p>
Indien een hardwareonderdeel van uw systeem <strong>>het laden van niet-vrije
firmware vereist</strong> voor het stuurprogramma van een apparaat, kunt u een
van de
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/lenny/current/">\
tar-archieven met gebruikelijke firmware-pakketten</a> gebruiken. Instructies
over het gebruik van deze tar-archieven en algemene informatie over het laden
van firmware tijdens de installatie is te vinden in de installatiehandleiding
(zie onder Documentatie hierna).
</p>

<p>
<strong>Opmerkingen</strong>
</p>
<ul>
#   <if-lennynhalf-released released="yes"><li>
#	Informatie over <strong>het installeren van Debian GNU/Linux
#	<q>lenny-eneenhalf</q></strong> (met een opgewaardeerde 2.6.24 kernel) is
#	te vinden op een <a href="lennynhalf">aparte pagina</a>.
#   </li></if-lennynhalf-released>
    <li>
	Voor het downloaden van volledige cd- of dvd-images wordt het gebruik van
	bittorrent of jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de cd- of dvd-set beschikbaar als ISO-bestand of via bittorrent.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	De multi-arch <em>cd</em>-images zijn respectievelijk bedoeld voor
    i386/amd64/powerpc en alpha/hppa/ia64; de installatie is vergelijkbaar met
    een installatie met een netinst-image voor één enkele architectuur.
    </li><li>
	Het multi-arch <em>dvd</em>-image is bedoeld voor i386/amd64; de
	installatie is vergelijkbaar met een installatie met een
	volledig cd-image voor één enkele architectuur. De dvd bevat ook
	al de broncode voor de opgenomen pakketten.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze
<a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Lenny Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met bekende problemen in het installatieprogramma dat met
Debian GNU/Linux <current_release_lenny> wordt geleverd. Indien u bij het
installeren van Debian op een probleem gestoten bent en dit probleem hier niet
vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u het probleem beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

<h3 id="errata-r0">Errata voor release 5.0</h3>

<dl class="gloss">
	<dt>Automatische assemblage van RAID-arrays in reddingsmodus kan gegevens
        beschadigen</dt>
	<dd>
        De reddingsmodus moet met de grootste zorg worden gebruikt wanneer
        software-RAID-arrays in gebruik zijn op het te redden systeem. De
        scripts voor de reddingsmodus assembleren arrays automatisch, wat kan
        leiden tot gegevensbeschadiging wanneer er ongeldige of verouderde
        RAID-superblokken aanwezig zijn.
	</dd>

	<dt>Beschadigde weergave van berichten bij installaties in het Dzongkha</dt>
	<dd>
	    Als het wachtwoord dat voor root is gekozen niet overeenkomt met de
        bevestiging, wordt de weergave van de volgende schermen vervormd
        tijdens de installatie in de Dzongkha-taal (beschadigde weergave van
        cursief lettertype).
	</dd>

	<dt>Schijfapparaten kunnen na het herstarten wijzigen</dt>
	<dd>
	Op systemen met meerdere schijfcontrollers kan de kernel/udev bij het
    herstarten van het systeem een andere apparaatnode toewijzen dan werd
    gebruikt tijdens de installatie, en dit vanwege een verschil in de volgorde
    waarin stuurprogramma's worden geladen.<br />
    Dit kan ertoe leiden dat het systeem niet kan worden opgestart. In de
    meeste gevallen kan dit worden gecorrigeerd door de configuratie van de
    bootloader en /etc/fstab te wijzigen, eventueel met behulp van de
    reddingsmodus van het installatieprogramma.<br />
	Merk echter op dat dit probleem opnieuw kan optreden wanneer een volgende
    keer opgestart wordt.
	</dd>

	<dt>Problemen met het herstarten wanneer vanaf een USB-stick geïnstalleerd
    wordt</dt>
	<dd>
	Het voorgaande probleem kan ook optreden bij installatie vanaf een
    USB-stick. Door de USB-stick tijdelijk op zijn plaats te houden, kunt u het
    geïnstalleerde systeem opstarten en het configuratiebestand van de
    bootloader corrigeren. Zie
	<a href="https://bugs.debian.org/506263">#506263</a>
	voor details over een dergelijke oplossingsmethode.
	</dd>

	<dt>Routers die bugs vertonen, kunnen netwerkproblemen veroorzaken</dt>
	<dd>
	Als u netwerkproblemen ondervindt tijdens de installatie, kan dit worden
    veroorzaakt door een router ergens tussen u en de Debian-spiegelserver die
    niet correct omgaat met <q>window scaling</q>.
	Zie <a href="https://bugs.debian.org/401435">#401435</a> en dit
	<a href="http://kerneltrap.org/node/6723">artikel van kerneltrap</a> voor
	details.<br />
	U kunt dit probleem omzeilen door <q>TCP window scaling</q> uit te zetten.
	Activeer een shell en geef het volgende commando:<br />
	<tt>echo 0 &gt; /proc/sys/net/ipv4/tcp_window_scaling</tt><br />
	Voor het geïnstalleerde systeem moet u <q>TCP window scaling</q>
    waarschijnlijk niet volledig uitschakelen. Met de volgende opdracht wordt
    een bereik voor lezen en schrijven ingesteld dat met bijna elke router zou
    moeten werken:<br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_rmem</tt><br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_wmem</tt>
	</dd>

	<dt>Niet bruikbaar om Squeeze of Sid te installeren</dt>
	<dd>
	Wegens veranderingen in het pakket <tt>passwd</tt> in <q>testing</q> en
    <q>unstable</q> zal het instellen van een gebruikersaccount mislukken.
	Zie voor details
	<a href="https://bugs.debian.org/529475">#529475</a>.
	</dd>

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

	<dt>i386: diverse problemen</dt>
	<dd>
	De architectuur i386 wordt in deze release met enkele gekende problemen
    geconfronteerd:
	<ul>
	<li>Door de toegenomen grootte van de Linux kernel zijn wij niet in staat
        om installatie-images te leveren voor installaties vanaf diskette.</li>
	<li>We hadden ten minste één rapport over het crashen van het
        installatieprogramma op sommige Dell Inspiron-laptops bij de stap
        waarin de netwerkhardware gedetecteerd wordt.
	    Zie <a href="https://bugs.debian.org/509238">bug #509238</a>
	    voor details. Het lijkt dat het probleem omzeild kan worden door het
        installatieprogramma op te starten met de parameter <q>vga=771</q>.</li>
	  </ul>
	</dd>

	<dt>PowerPC: diverse problemen</dt>
	<dd>
	De architectuur PowerPC wordt in deze release met verschillende problemen
    geconfronteerd:
	<ul>
		<li>installeren vanaf diskette is defect op OldWorld PowerMac
            omdat er geen apparaatnode wordt gemaakt voor de swim3-module en
            omdat miboot niet is opgenomen</li>
		<li>de module snd-powermac wordt niet langer standaard geladen omdat
            deze sommige systemen zal doen vastlopen; u zult ze handmatig
		    moeten toevoegen aan <tt>/etc/modules</tt></li>
	</ul>
	</dd>

	<dt>s390: niet-ondersteunde functionaliteit</dt>
	<dd>
	<ul>
		<li>ondersteuning voor de DASD DIAG discipline is momenteel niet
		    beschikbaar</li>
		<li>er wordt niet langer ondersteuning geboden voor
            LCS netwerkinterfaces</li>
	</ul>
	</dd>
</dl>
