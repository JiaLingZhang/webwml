<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in bind9, the Internet Domain Name Server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6465">CVE-2019-6465</a>

    <p>Zone transfer for DLZs are executed though not permitted by ACLs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5745">CVE-2018-5745</a>

    <p>Avoid assertion and thus causing named to deliberately exit when a
    trust anchor's key is replaced with a key which uses an unsupported
    algorithm.</p></li>

</ul>

<p>For Debian 8 &quot;Jessie&quot;, these problems have been fixed in version
1:9.9.5.dfsg-9+deb8u17.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1697.data"
# $Id: $
