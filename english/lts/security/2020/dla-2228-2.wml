<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The json-c shared library had an integer overflow and out-of-bounds write
via a large JSON file, as demonstrated by printbuf_memappend.</p>

<p>This follow-up version now uses an upstream sanctioned patch that was
specifically published for json-c 0.11, rather than a self-backported
patch.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.11-4+deb8u2.</p>

<p>We recommend that you upgrade your json-c packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2228-2.data"
# $Id: $
