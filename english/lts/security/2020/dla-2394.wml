<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in Squid, a highperformance proxy caching server for web clients.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15049">CVE-2020-15049</a>

    <p>An issue was discovered in http/ContentLengthInterpreter.cc in
    Squid. A Request Smuggling and Poisoning attack can succeed against
    the HTTP cache. The client sends an HTTP request with a Content    Length header containing "+\ "-" or an uncommon shell whitespace
    character prefix to the length field-value.
    This update also includes several other improvements to the
    HttpHeader parsing code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15810">CVE-2020-15810</a>

<p>and <a href="https://security-tracker.debian.org/tracker/CVE-2020-15811">CVE-2020-15811</a></p>

    <p>Due to incorrect data validation, HTTP Request Smuggling attacks may
    succeed against HTTP and HTTPS traffic. This leads to cache
    poisoning and allows any client, including browser scripts, to
    bypass local security and poison the proxy cache and any downstream
    caches with content from an arbitrary source. When configured for
    relaxed header parsing (the default), Squid relays headers
    containing whitespace characters to upstream servers. When this
    occurs as a prefix to a Content-Length header, the frame length
    specified will be ignored by Squid (allowing for a conflicting
    length to be used from another Content-Length header) but relayed
    upstream.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24606">CVE-2020-24606</a>

    <p>Squid allows a trusted peer to perform Denial of Service by
    consuming all available CPU cycles during handling of a crafted
    Cache Digest response message. This only occurs when cache_peer is
    used with the cache digests feature. The problem exists because
    peerDigestHandleReply() livelocking in peer_digest.cc mishandles
    EOF.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.23-5+deb9u5.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>For the detailed security status of squid3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2394.data"
# $Id: $
